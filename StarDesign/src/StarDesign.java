
public class StarDesign
{

	public static void main(String[] args) 
	{
		int length = 1;
		int height = 1;
		
		
		while (height<=8)
		{
			//if statement to add a space in front of even lines
			if (height%2!=1)
			{
				System.out.print(" ");
			}
			
			//a line of 8 stars
			while(length<=8)
			{
				System.out.print("* ");
				length++;
			}
			System.out.println();// new line
			height++;
			length = 1; // reset inner loop -----------
		}
	}//end main method

}
